<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

//librería de validaciones:
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Animales
 *
 * @ORM\Table(name="animales")
 * @ORM\Entity(repositoryClass="App\Repository\AnimalRepository")
 */
class Animal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="especie", type="string", length=255, nullable=false)
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/[a-zA-Z]+/", message="La especie tiene que ser una cadena solamente de letras y espacios")
     */
    private $especie;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=false)
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/[a-zA-Z]+/", message="El color tiene que ser una cadena solamente de letras y espacios")
     * 
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="pais", type="string", length=255, nullable=false)
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/[a-zA-Z]+/", message="El país tiene que ser una cadena solamente de letras y espacios")
     */
    private $pais;

    public function getId()
    {
        return $this->id;
    }

    public function getEspecie()
    {
        return $this->especie;
    }

    public function setEspecie(string $especie): self
    {
        $this->especie = $especie;

        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getPais()
    {
        return $this->pais;
    }

    public function setPais(string $pais): self
    {
        $this->pais = $pais;

        return $this;
    }


}
