<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FutbolistaRepository")
 */
class Futbolista {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false )
     */
    private $nombre;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=false )
     */
    private $demarcacion;
    
    /**
     * @ORM\Column(type="integer", length=11, nullable=false )
     */
    private $dorsal;
    
    /**
     * @ORM\Column(type="integer", length=11, nullable=true )
     */
    private $edad;
    
    /**
     * @ORM\Column(type="integer", length=11, nullable=true )
     */
    private $goles;
    
    /**
     * @ORM\Column(type="integer", length=11, nullable=true )
     */
    
    private $ficha;

    public function getId() {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDemarcacion()
    {
        return $this->demarcacion;
    }

    public function setDemarcacion(string $demarcacion)
    {
        $this->demarcacion = $demarcacion;

        return $this;
    }

    public function getDorsal()
    {
        return $this->dorsal;
    }

    public function setDorsal(int $dorsal)
    {
        $this->dorsal = $dorsal;

        return $this;
    }

    public function getEdad()
    {
        return $this->edad;
    }

    public function setEdad(int $edad)
    {
        $this->edad = $edad;

        return $this;
    }

    public function getGoles()
    {
        return $this->goles;
    }

    public function setGoles(int $goles)
    {
        $this->goles = $goles;

        return $this;
    }

    public function getFicha()
    {
        return $this->ficha;
    }

    public function setFicha(int $ficha)
    {
        $this->ficha = $ficha;

        return $this;
    }

}
