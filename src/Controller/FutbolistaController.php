<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;



use Symfony\Component\HttpFoundation\Request;


use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;

use App\Entity\Futbolista;

class FutbolistaController extends AbstractController
{
    
     /**
     * @Route("/futbolista/crear", name="futbolista_crear")
     */   
    public function crear(){
        
        $futbolista=new Futbolista;
        $futbolista=$this->createFormBuilder($futbolista)
                ->setAction($this->generateUrl('futbolista_crear'))
                ->setMethod('POST')
                ->add('nombre', TextType::class)
                ->add('demarcacion', TextType::class)
                ->add('dorsal', NumberType::class)
                ->add('edad', NumberType::class)
                ->add('goles', NumberType::class)
                ->add('ficha', TextType::class)
                ->add('enviar', SubmitType::class)
                ->getForm();
        
        
        return $this->render('futbolista/crear-form.html.twig',['form'=>$futbolista->createView()]);
    }
    
    
    public function validarEmail($email){
        //En este método validamos un dato aislado, un email que pasamos por la url.
        
        $validator= Validation::createValidator();
        $errores=$validator->validate($email,[new Email()]);
        
        if(count($errores)!=0){
            $message="El email NO ES VÁLIDO";
            
        }else{
            $message="El email introducido es correcto";
        }
        
        echo $message;
        var_dump($errores);
        die();       
        
    }
    
    
    /**
     * @Route("/futbolista", name="futbolista")
     */
    public function index()
    {
        return $this->render('futbolista/index.html.twig', [
            'controller_name' => 'FutbolistaController',
        ]);
    }
}
