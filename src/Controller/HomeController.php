<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController {

    /**
     * @Route("/home", name="home")
     */
    public function index() {
        return $this->render('home/index.html.twig', [
                    'controller_name' => 'HomeController',
                    'hola' => 'Hola mundo en Symfony 4',
        ]);
    }

    public function animales($animal, $nombre, $telefono) {
        $titulo = 'Página de animales';

        $animales = ['vaca', 'caballo', 'conejo', 'cerdo'];

        $caballo = [
            'raza' => 'portugués',
            'edad' => 6,
            'peso' => 190,
            'sexo' => 'macho',
        ];


        return $this->render('home/animales.html.twig', [
                    'titulo' => $titulo,
                    'animal' => $animal,
                    'nombre' => $nombre,
                    'telefono' => $telefono,
                    'animales' => $animales,
                    'caballo' => $caballo,
        ]);
    }

    public function usuarios($nombre) {

        $datos = ['ciudad' => 'Santander',
            'provincia' => 'Cantabria',
            'direccion' => 'C/ Floranes 8'];

        return $this->render('home/usuarios.html.twig', [
            'nombre' => $nombre,
            'datos' => $datos,
            ]);
    }

    //Redireccionar a una ruta interna (método redirectToRoute), llama al nombre de la ruta en routes.yaml

    public function redirigir() {

        return $this->redirectToRoute('animales');
    }

    //Redireccionar a una ruta absoluta (método redirect)
    public function miweb() {

        return $this->redirect('http://www.albertoceballos.es');
    }

    //todas las rutas hay que llamarlas en el archivo routes.yaml, las rutas normales y las redireciones también.
}
