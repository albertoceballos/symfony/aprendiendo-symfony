<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;



//clase necesaria para recibir datos por POST:
use Symfony\Component\HttpFoundation\Request;

//clases para construir los campos de formularios:
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



//Cargar la entidad(modelo)
use App\Entity\Animal;

class AnimalController extends AbstractController {
    
    public function crearAnimal(Request $request){
        
        $animal=new Animal;
        $form= $this->createFormBuilder($animal)
                //->setAction($this->generateUrl('animal_save'))
                ->setMethod('POST')
                ->add('especie', TextType::class)
                ->add('color', TextType::class)
                ->add('pais', TextType::class,['label'=>'País de Origen'])
                ->add('Enviar', SubmitType::class,[
                    'attr'=>['class'=>'btn btn-enviar']
                    ])
                ->getForm();
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager=$this->getDoctrine()->getManager();
            $entityManager->persist($animal);
            $entityManager->flush();
            
            $session=new Session();
            
            $session->getFlashBag()->add('mensaje','animal creado');
            
            return $this->redirectToRoute('animal_crear_form');
        }
                       
        return $this->render('animal/crear-animal.html.twig',['form'=>$form->createView()]);
    }
    

    public function index() {
        //Voy a mostrar todos los aniamles de la tabla
        //Primero cargo el repositorio
        $animal_repo = $this->getDoctrine()->getRepository(Animal::class);

        //Consulta
        $animales = $animal_repo->findAll();

        $animals = $animal_repo->ordenarPorColor();

        var_dump($animals);

        return $this->render('animal/index.html.twig', [
                    'controller_name' => 'AnimalController',
                    'animales' => $animales,
        ]);
    }

    public function save() {
        //Guardar en la BB.DD


        $entityManager = $this->getDoctrine()->getManager();

        $animal = new Animal;

        $animal->setEspecie('Elefante');
        $animal->setColor('gris');
        $animal->setPais('Camerún');

        //Guardar objeto en Doctrine
        $entityManager->persist($animal);

        //guardar obejtos persistidos en Doctrine en la BB.DD
        $entityManager->flush();


        return new Response('Datos guardados');
    }

    public function detail($id) {
        //En este método vamos a sacar un animal por su ID
        //primero cargamos el repositorio
        $animal_repo = $this->getDoctrine()->getRepository(Animal::class);

        //hacemos la consulta
        $animal = $animal_repo->find($id);

        if (!$animal) {
            $respuesta = "El animal no existe";
        } else {
            $respuesta = "El animal es buscado es " . $animal->getEspecie() . " de color: " . $animal->getColor() . ", y de origen: " . $animal->getPais();
        }

        return new Response($respuesta);
    }

    public function busca_pais($pais) {
        //función que me busca todos los animales de un país determinado, (método findBy)

        $animal_repo = $this->getDoctrine()->getRepository(Animal::class);

        $animal = $animal_repo->findBy(
                //el primer parametro es por el que busca, como si fuera un WHERE
                ['pais' => $pais],
                //el segundo parametro es por el que se puede ordenar como si fuera un ORDER BY
                ['especie' => 'ASC']
        );

        if (!$animal) {
            $respuesta = "No hay animales de ese país";
        } else {
            $respuesta = var_dump($animal);
        }

        return new Response($respuesta);
    }

    public function busca_color($color) {
        //función que saca solo el primer animal que se busca por un color ( método findByOne) como si fuera un WHERE con un LIMIT 1


        $animal_repo = $this->getDoctrine()->getRepository(Animal::class);

        $animal = $animal_repo->findOneBy(['color' => $color], ['pais' => 'ASC']);

        if (!$animal) {
            $respuesta = "No existe ningún animal de ese color";
        } else {
            $respuesta = var_dump($animal);
        }

        return new Response($respuesta);
    }

    public function update($id) {
        //método para actualizar registros
        //cargamos el repositorio para poder usar el find después
        $animal_repo = $this->getDoctrine()->getRepository(Animal::class);

        //cargamos el manager de doctrine para después poder guardar en la BBDD
        $entityManager = $this->getDoctrine()->getManager();

        //hacemos la consulta
        $animal = $animal_repo->find($id);

        //llamamos a los métodos set para asignar nuevos valores
        $animal->setEspecie('Oso pardo');
        $animal->setColor('marrón');
        $animal->setPais('Rusia');

        //guardamos el objeto temporalmente en doctrine con persist
        $entityManager->persist($animal);

        //guardamos en la BBDD lo que hemos persistido antes
        $entityManager->flush();

        return new Response("Se ha actualizado el animal $id");
    }

    public function delete(Animal $animal) {

        //$animal=$this->getDoctrine()->getRepository(Animal::class)->find($id);
        $em = $this->getDoctrine()->getManager();

        if ($animal && is_object($animal)) {

            $em->remove($animal);
            $em->flush();
            $respuesta = "Animal borrado";
        } else {
            $respuesta = "Animal no encontrado";
        }
        return new Response($respuesta);
    }

    public function ordenaColor() {
        //Llano a la consulta creada por mi en el repositorio y la muestro en una vista

        $animal_repo = $this->getDoctrine()->getRepository(Animal::class);

        $animales = $animal_repo->ordenarPorColor();

        return $this->render('animal/ordencolores.html.twig', ['animales' => $animales]);
    }

    public function buscaPalabra($palabra) {
        //Consulta creada con DQL , como SQL pero trabaja con las entidades

        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM App\Entity\Animal a WHERE a.especie LIKE '%$palabra%'"
                . " or a.color LIKE '%$palabra%'"
                . " or a.pais LIKE '%$palabra%' ";
        $consulta = $em->createQuery($dql);
        $animales = $consulta->execute();

        if ($animales) {
            $resultado = $animales;
        } else {
            $resultado = "No hay coincidencias";
        }

        return new Response(var_dump($resultado));
    }

    public function ordena_pais() {
        //ordenar todos por pais descendente. Creada con SQL
        
        $connect = $this->getDoctrine()->getConnection();
        $sql = "SELECT * FROM animales ORDER BY pais DESC";
        $prepare = $connect->prepare($sql);
        $prepare->execute();
        $resultset = $prepare->fetchAll();


        return new Response(var_dump($resultset));
    }
    
    
    public function buscar_pais($pais){
        //Buscar por país. Consulta creada en el repositorio, ejecutada con DQL.
        
        $animal_repo= $this->getDoctrine()->getRepository(Animal::class);
        
        $animales=$animal_repo->buscarPais($pais);
        
        if($animales){
            $datos=$animales;
            
        }else{
            $datos="No hay ningún animal de ese país";
        }
        
        return $this->render('animal/buscarpais.html.twig',['datos'=>$datos]);
        
    }

}
