<?php

namespace App\Repository;

use App\Entity\Animal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


Class AnimalRepository extends ServiceEntityRepository{
    
    public function __construct(RegistryInterface $registry) {
        
        parent::__construct($registry, Animal::class);
    }
    
    
    public function ordenarPorColor(){
     //consulta creada con QueryBuilder
        
        $qb= $this->createQueryBuilder('a')
                ->orderBy('a.color','ASC')
                ->getQuery();
        
        $resultado=$qb->execute();
        return $resultado;
    }
             
    
    public function buscarPais($pais){
        //funcion para buscar por pais. Creada con DQL
        
        $entityManager= $this->getEntityManager();
        $dql="SELECT a FROM App\Entity\Animal a WHERE a.pais=:pais";
        $query=$entityManager->createQuery($dql)
                ->setParameter('pais',$pais);
     
        $resultset =$query->execute();
        
        return $resultset;
    }
    
    public function ultimoRegistro(){
        
        
        $entityManager=$this->getEntityManager();
        $connection=$entityManager->getConnection();
        $sql="SELECT * FROM animales ORDER BY id DESC LIMIT 1";
        $prepare=$connection->prepare($sql);
        $prepare->execute();
        $resultset=$prepare->fetchAll();
        
        return $resultset;
    }
}




