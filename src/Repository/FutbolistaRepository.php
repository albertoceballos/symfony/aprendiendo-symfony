<?php

namespace App\Repository;

use App\Entity\Futbolista;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Futbolista|null find($id, $lockMode = null, $lockVersion = null)
 * @method Futbolista|null findOneBy(array $criteria, array $orderBy = null)
 * @method Futbolista[]    findAll()
 * @method Futbolista[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FutbolistaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Futbolista::class);
    }

    // /**
    //  * @return Futbolista[] Returns an array of Futbolista objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Futbolista
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
