<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class MiExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filtro_multiplicar', [$this, 'multiplicar']),
            new TwigFilter('sumar',[$this,'suma']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('funcion_multiplicar', [$this, 'multiplicar']),
            new TwigFunction('sumar',[$this,'suma']),
        ];
    }

    public function multiplicar($numero)
    {
        
       $tabla="<h1>Tabla del ".$numero."</h1>";
       for($i=0;$i<=10;$i++){
           
           $tabla.=$i." x ".$numero."= ".($numero*$i)."<br>";
       }
    
       return $tabla;
    }
    
    public function suma($num1,$num2){
        
        $resultado=$num1+$num2;
        
        return $resultado;
    }
    
}
